<div class="audio clearfix" style="direction: ltr;">
  <?php if ($settings['view_link']): ?>
    <a class="view" title="<?php print t('View'); ?>" href="<?php print url($entity_uri['path']); ?>"><?php print t('View'); ?></a>
  <?php endif; ?>
  <?php if ($settings['download_link']): ?>
    <a class="download" title="<?php print t('Download'); ?>" href="<?php print $music_download_url; ?>"><?php print t('Download'); ?></a>
  <?php endif; ?>
  <?php if ($settings['inline_player']): ?>
    <audio src="<?php print $music_url; ?>" preload="auto"></audio>
  <?php endif; ?>
</div>
